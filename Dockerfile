FROM registry.gitlab.com/joejulian/docker-arch:latest

RUN pacman -Syu --noconfirm clamav && pacman -Scc --noconfirm

RUN mkdir -m 777 -p /config /socket /data

COPY example/clamd.conf /data/clamd.conf

RUN freshclam --stdout --user clamav --update-db=main --update-db=daily --update-db=bytecode --update-db=safebrowsing --datadir=/data

CMD clamd -c /config/clamd.conf
