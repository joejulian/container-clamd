# clamd container

clamd will need to be in the same mnt namespace as the files it's being asked
to scan, and those files will probably need to be at the same path from root.

It's also probably a good idea to use the same ipc namespace so clamd can be
intefaced with via its socket.

## Usage

A sample pod using clamd
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: amavisd
  labels:
    app: amavisd
spec:
  replicas: 1
  selector: 
    matchLabels
      app: amavisd
  template:
    metadata:
      labels:
        app: amavisd
    spec:
      containers:
        - name: clamd
          image: registry.gitlab.com/joejulian/container-clamd:latest
          imagePullPolicy: Always
          volumeMounts:
            - name: clamd-conf
              mountPath: /config
            - name: socket
              mountPath: /socket
            - name: workdir
              mountPath: /workdir
        - name: amavisd-new
          image: registry.gitlab.com/joejulian/container-amavisd-new:latest
          imagePullPolicy: Always
          volumeMounts:
            - name: amavisd-new
              mountPath: /config
            - name: socket
              mountPath: /socket
            - name: workdir
              mountPath: /workdir
      volumes:
        - name: clamd-conf
          configMap:
            name: clamd-conf
        - name: amavisd-new
          configMap:
            name: amavisd-new
        - name: workdir
          emptyDir: {}
        - name: socket
          emptyDir: {}
```

NOTE: Notice the shared `socket` emptydir. This is where the unix socket will
be created by clamd and consumed by amavisd
